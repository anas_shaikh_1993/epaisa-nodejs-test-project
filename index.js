const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql2');
const app = express();

const port = 3000;
const error_code = 202;
const success_code = 200;

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// create the connection to database
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'epaisa-test'
});

app.get('/', (req, res) => {
  res.send('Hello World!');
});


function getAllQuestions(){
    return new Promise((resolve,reject) => {
        let queryString = 'SELECT * FROM `questions`';
        connection.query(queryString,(err, results) => {
            if(err)
            {
               return reject(err);
            }
            return resolve(results);
        })
    })
}

function getAllOptionsForQuestions(questionIds = [],questions = {}){

    return new Promise((resolve,reject) => {
            if(questionIds.length == 0)
            {
                return reject("question id array found empty");
            }
            
            let queryString = 'SELECT * FROM `options` where questionId in (?)';
            let queryData = [questionIds];
            connection.query(queryString, queryData, function (err, results) {
                    if(err){return reject(err);}
                    results.forEach(item => {
                        let obj = {'id':item['id'],'questionId':item['questionId'],'option':item['optiontxt']}
                        if(questions[item['questionId']]['id'] == obj['questionId'])
                        {
                            questions[item['questionId']]['options'].push(obj);
                        }
                        
                    });
                    return resolve({'questionIds':questionIds,'questions':questions});
            });
    })
}

function getAllAnswersForQuestions(questionIds = [],questions = {}){
    return new Promise((resolve,reject) => {
            if(questionIds.length == 0)
            {
                return reject("question id array found empty");
            }

            let queryString = 'SELECT * FROM `options` where questionId in (?)';
            let queryData = [questionIds];
            connection.query(queryString,queryData,(err,results) => {
                if(err){return reject(err);}
                results.forEach(item => {
                    if(item['questionId'] == questions[item['questionId']]['id'])
                    {
                        questions[item['questionId']]['answer'] = {'questionId':item['questionId'],'optionId':item['optionId']};
                    }
                });
                return resolve(questions);
            });
    });
}

//function to check if user already exists
function checkUserExist(emailId){
    return new Promise((resolve,reject) => {
        let queryString = "SELECT count(*) as result FROM `user` where email = (?)"  ;
        let queryData = [emailId]
        connection.query(queryString,queryData,(err, results) => {
            if(err)
            {
               return reject(err);
            }
            return resolve(results);
        })
    });
};

function InsertUser(emailId){
    return new Promise((resolve,reject) => {
        let sql = "INSERT INTO user (email) VALUES ?";
        let values = [[emailId]]
        connection.query(sql,[values],(err,results) => {
            if(err) reject(err)
            resolve({'rows':results.affectedRows,'id':results.insertId});
        });
    });
    
}

function getTotalScoreOfUser(userId){
    return new Promise((resolve,reject) => {
        let queryString = "SELECT * FROM `user_score` where userId = (?)"  ;
        let queryData = [userId]
        connection.query(queryString,queryData,(err, results) => {
            if(err)
            {
               return reject(err);
            }
            return resolve(results);
        })
    })
}

function checkIfAnswerIsRight(questionId,optionId){
    return new Promise((resolve,reject) => {
        let queryString = "SELECT count(*) as result FROM `answers` where questionId = (?) and optionId = (?)";  ;
        let queryData = [questionId,optionId]
        connection.query(queryString,queryData,(err, results) => {
            if(err)
            {
               return reject(err);
            }
            return resolve(results);
        })
    });
}

function insertScore(userId,score){
    return new Promise((resolve,reject) => {
        let sql = "INSERT INTO user_score (userId,score) VALUES ?";
        let values = [[userId,score]]
        connection.query(sql,[values],(err,results) => {
            if(err) reject(err)
            resolve({'rows':results.affectedRows,'id':results.insertId});
        });
    });
}

function updateScore(userId,score){
    return new Promise((resolve,reject) => {
        let sql = "UPDATE user_score SET score = ? WHERE userId = ?";
        let values = [score,userId]
        connection.query(sql,values,(err,results) => {
            if(err) reject(err)
            resolve({'rows':results.affectedRows});
        });
    });
}

function IncrementUserScore(userId){
    return new Promise((resolve,reject) => {
        getTotalScoreOfUser(userId).then(r => {
            if(r.length == 0)
            {
                //insert into table
                return insertScore(userId,100);
            }    
            else{
                let score = r[0]['score'] + 100;
                return updateScore(userId,score) 
            }
        })
        .then(r => {
            return resolve({'incremented':true})
        })
        .catch(err => {
            return reject(err);
        })
    });
}

app.get('/questions',(req,res) => {
     
   const result =  getAllQuestions().then( r => {
       let questionIds = [];
           
       let questions = [];
        r.forEach(element => {
            questionIds.push(element['id']); 
            questions[element['id']] = {'id':element['id'],'question':element['question'],'options':[],'answer':{}};
        });
        // console.log(questions)
        // return {'code':success_code,'data':[]}
        
        return getAllOptionsForQuestions(questionIds,questions);
        
   })
   .then(r => {
        let questionIds = r['questionIds'];
        let questions = r['questions'];
        return getAllAnswersForQuestions(questionIds,questions);
       
    })
    .then(r => {
        let data = [];
        Object.keys(r).forEach(key => {
            data.push(r[key]);
        })
        return {'code':success_code,'data':data,'err':null}
    })
   .catch(err => {
       console.error(err)
       return {'code':error_code,'data':[],'err':err}
   })
   result.then(r => res.send(r)).catch(e => res.send(e));
   
});

app.post('/user',(req,res) => {
   let userObj = req.body;
   if(!userObj['emailId'])
   {
       return res.send({'code':error_code,'data':{},'err':"emailId Not Provided"})
   } 

   const queryResult = checkUserExist(userObj['emailId']);
   queryResult.then(r => {
    //    console.log(r);
        if(r[0]['result'] > 0)
        {
         throw "user already exists";   
        }
        return InsertUser(userObj['emailId']);
   })
   .then(r => {
       console.log(r);
    return res.send({'code':200,'data':{'id':r['id']}});
   })
   .catch(e => {
        return res.send({'code':error_code,'data':{},'err':e})
   });

});

app.get('/getTotalScore/:userId',(req,res) => {
    let userId = req.params['userId'];
    const result = getTotalScoreOfUser(userId);
    result.then(r => {
        let obj = {};
        r.forEach(ele => { obj['userId'] = ele['userId']; obj['totalScore'] =  ele['score'];});
        
        return res.send({'code':success_code,'data':obj,'err':{}});
    }).catch(err => {
        return res.send({'code':error_code,'data':{},'err':err});
    });
})

app.post('/submitAnswer',(req,res) => {
    let userId = req['body']['userId'];
    let questionId = req['body']['questionId'];
    let optionId = req['body']['optionId'];
    const result = checkIfAnswerIsRight(questionId,optionId);
    result.then(r => {
        if(r[0]['result'] == 1)
        {
            return IncrementUserScore(userId);
        }else{
            return Promise.resolve({'incremented':false})
        }
        
        
    })
    .then(r => {
        return res.send({'code':success_code,'data':r,'err':{}});
    })
    .catch(err => {
        console.error(err);
        return res.send({'code':error_code,'data':{},'err':err});
    })
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});